= DIDAFF for Devuan

This project holds the implementation of a Distributed Intrusion
Detection And Firewalling Framework (DIDAFF) on the Devaun
infrastructure. This infrastructure comprises a small group of
bare-metal nodes that host a range of virtual machine based services,
using the "ganeti" VPS platform.

The DIDAFF includes coordinated firewall setup for the nodes based on
a replicated Baddies Database that is feed from the distributed
detection sources using a combination of tools such as +fail2ban+,
+sshguard+ and bespoke scripting. All detection sources link up with
and provide entries for the Baddies Database in their own ways, and
this is replicated onto the nodes for them to use their replica as
basis for managing the firewall.

Each node runs a DIDAFF server as well as the firewall cron bot.
Various virtual machines have detection logic and tells the nodes
about "baddies" through the DIDAFF API, which uses broadcast on the
local net.

