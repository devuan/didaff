# Firewall configuration

SET=DIDAFF
TIMEOUT=10800 # 3 hours

# How to add the iptables rule(s) using the set
iptables_add() {
    iptables -I FORWARD -i eth0  -m set --match-set $SET src -j DROP
}

# How to remove the iptables rule(s) using the set
iptables_del() {
    iptables -D FORWARD -i eth0  -m set --match-set $SET src -j DROP
}

# How to add the nominated set with the given default timeout
ipset_add() {
    ipset create $SET hash:net timeout $TIMEOUT
}

# How to remove a nominated set
ipset_del() { # set
    ipset destroy $SET
}

firewall_enable() {
    ipset_add
    iptables_add
}

firewall_disable() {
    iptables_del
    ipset_del
}

cron_enable() {
    cat <<EOF >> /etc/cron.d/firewall
* * * * * $(dirname $0)/firewall-cron
EOF
}

cron_disable() {
    rm -f /etc/cron.d/firewall
}

set -x
case "$1" in
    enable)
	ipset_add
	iptables_add
	cron_enable
	;;
    disable)
	cron_disable
	iptables_del
	ipset_del
	;;
esac
